package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

type marker struct {
	FileName string `json:"fn"`
	Start    int    `json:"st"`
	Length   int    `json:"len"`
}

func getIndex(filename string) (map[string]marker, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	ndxLenStart := len(bytes) - 8
	ndxLenBytes := bytes[ndxLenStart:]
	ndxLen := binary.LittleEndian.Uint64(ndxLenBytes)
	start := len(bytes) - (8 + int(ndxLen))
	ndx := bytes[start:ndxLenStart]
	var markers []marker
	err = json.Unmarshal(ndx, &markers)
	indexMap := make(map[string]marker)
	for _, m := range markers {
		indexMap[m.FileName] = m
	}
	return indexMap, nil
}

func main() {
	source := flag.String("source", "bundle.bin", "Source")
	unpack := flag.String("unpack", "go.mod", "Name of the file to unpack")
	target := flag.String("target", "go.mod_copy", "Name of the file to generate")
	flag.Parse()
	ndx, err := getIndex(*source)
	if err != nil {
		fmt.Printf("Error reading index: %s\n", err)
		os.Exit(1)
	}
	marker := ndx[*unpack]
	fileBytes, err := ioutil.ReadFile(*source)
	if err != nil {
		fmt.Printf("Error reading binary file: %s\n", err)
		os.Exit(1)
	}
	newFile := fileBytes[marker.Start : marker.Start+marker.Length]
	ioutil.WriteFile(*target, newFile, 444)
}
