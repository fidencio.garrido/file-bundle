package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type marker struct {
	FileName string `json:"fn"`
	Start    int    `json:"st"`
	Length   int    `json:"len"`
}

func isCandidate(file string, info os.FileInfo) bool {
	chrs := []byte(file)
	if len(file) == 0 || chrs[0] == 46 {
		return false
	}
	if info.IsDir() {
		return false
	}
	return true
}

func bundle(source string, output string) error {
	buffer := []byte{}
	index := make([]marker, 0)
	start := 0
	err := filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if isCandidate(path, info) {
			b, berr := ioutil.ReadFile(path)
			if berr == nil {
				buffer = append(buffer, b...)
				index = append(index, marker{path, start, len(b)})
				start += len(b)
			}
		}
		return nil
	})
	indexBytes, _ := json.Marshal(index)
	buffer = append(buffer, indexBytes...)
	indexLen := len(indexBytes)
	allocator := make([]byte, 8)
	binary.LittleEndian.PutUint64(allocator, uint64(indexLen))
	buffer = append(buffer, allocator...)
	ioutil.WriteFile(output, buffer, 444)
	return err
}

func main() {
	folder := flag.String("folder", ".", "Input folder")
	output := flag.String("output", "bundle.bin", "Output file")
	flag.Parse()
	fmt.Printf("Producing %s from %s\n", *output, *folder)
	err := bundle(*folder, *output)
	if err != nil {
		fmt.Printf("Failed: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("File created successfully")
}
